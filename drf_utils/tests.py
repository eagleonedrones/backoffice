from django.test import TestCase, Client as ClientBase
from django_extensions.management.commands.dumpscript import get_models
from itertools import combinations
from rest_framework.authtoken.models import Token

from contrib.helpers import create_dev_superuser


class Client(ClientBase):
    def __init__(self, user):
        self.content_type = 'application/json'

        token = Token.objects.get_or_create(user_id=user.id)[0]
        super(Client, self).__init__(HTTP_AUTHORIZATION=f"Token {token}")

    def get(self, path, data=None, content_type=None, **kwargs):
        return super(Client, self).get(path, data, content_type=content_type or self.content_type)

    def post(self, path, data=None, content_type=None, **kwargs):
        return super(Client, self).post(path, data, content_type=content_type or self.content_type)

    def put(self, path, data=None, content_type=None, **kwargs):
        return super(Client, self).put(path, data, content_type=content_type or self.content_type)

    def patch(self, path, data=None, content_type=None, **kwargs):
        return super(Client, self).patch(path, data, content_type=content_type or self.content_type)

    def delete(self, path, data=None, content_type=None, **kwargs):
        return super(Client, self).delete(path, data, content_type=content_type or self.content_type)


class CrudTestCase(TestCase):
    def setUp(self):
        self.user = create_dev_superuser()
        self.client = Client(self.user)


class GenericLookupsTestCase(CrudTestCase):
    def setUp(self):
        super(GenericLookupsTestCase, self).setUp()

        # let's generate all data we can get for free...
        for Model in get_models(None):
            if hasattr(Model, 'get_dummy'):
                Model.get_dummy()

        # these variables may be defined
        #
        self.defaults = {}
        self.skip_dummy_for = set()
        self.test_response_attrs = {}
        self.entity = None
        self.lookup_fields = set()
        self.api_endpoint = '/'
        self.response_should_be_empty = False

    @staticmethod
    def _get_query(query_dict):
        result = {}
        for key, value in query_dict.items():
            if type(value) == list and len(value) == 1 and type(value[0]) == list:
                result[key] = value[0]
            else:
                result[key] = ','.join(str(item) for item in value)
        return result

    @staticmethod
    def _get_combinations(iterable):
        result = []
        for n in range(len(iterable)):
            # N items of current combinations
            n += 1
            result.extend(combinations(iterable, n))
        return tuple(result)

    def _get_attr(self, attr_name):
        if attr_name in self.defaults:
            return self.defaults[attr_name]
        else:
            return getattr(self.entity, attr_name)

    def _get_endpoint(self, query):
        return self.api_endpoint

    def test_simple_lookup(self):
        """
            Iterates over all lookup fields searching for single value taken from given entity.
        """
        for attr in self.lookup_fields:
            value = self._get_attr(attr)
            query = self._get_query({attr: [value]})
            response = self.client.get(self._get_endpoint(query), query)

            results = self._get_results(response)
            self._check_results_length(results, query, self.response_should_be_empty)

            cb = self.test_response_attrs.get(attr, None)
            for item in results:
                if cb:
                    cb(self, item, value)
                else:
                    self.assertEqual(value, item[attr])

    def test_dummy_lookup(self):
        """
            Iterates over all lookup fields searching for dummy values that should return no results.
        """
        for attr in self.lookup_fields:
            value = self._get_attr(attr)
            if type(value) in {int, float}:
                query = self._get_query({attr: [42]})
            else:
                query = self._get_query({attr: ['DUMMY']})
            response = self.client.get(self._get_endpoint(query), query)

            results = self._get_results(response)
            self._check_results_length(results, query, response_should_be_empty=True)

            cb = self.test_response_attrs.get(attr, None)
            for item in results:
                if cb:
                    cb(self, item, value)
                else:
                    self.assertEqual(value, item[attr])

    def test_dummy_combined_lookup(self):
        """
            Iterates over all lookup fields, except ones defined to be skipped, searching for two
            values - dummy (non-existing) one and valid one - taken from given entity.
        """
        for attr in self.lookup_fields:
            if attr in self.skip_dummy_for:
                continue

            value = self._get_attr(attr)
            if type(value) in {int, float}:
                query = self._get_query({attr: [42, value]})
            else:
                query = self._get_query({attr: ['DUMMY', value]})
            response = self.client.get(self._get_endpoint(query), query)

            results = self._get_results(response)
            self._check_results_length(results, query, self.response_should_be_empty)

            cb = self.test_response_attrs.get(attr, None)
            for item in results:
                if cb:
                    cb(self, item, value)
                else:
                    self.assertEqual(value, item[attr])

    def test_lookup_combinations(self):
        """
            Iterates over all lookup fields combination searching for values taken from given entity.
        """
        for attrs in self._get_combinations(self.lookup_fields):
            query = {
                attr: [self._get_attr(attr)]
                for attr in attrs
            }

            _query = self._get_query(query)
            response = self.client.get(self._get_endpoint(_query), _query)

            results = self._get_results(response)
            self._check_results_length(results, query, self.response_should_be_empty)

            for item in results:
                for attr, value in query.items():
                    cb = self.test_response_attrs.get(attr, None)
                    if cb:
                        cb(self, item, value[0])
                    else:
                        self.assertEqual(value[0], item[attr])

    @staticmethod
    def _get_results(response):
        if response.status_code != 200:
            raise AssertionError('Wrong status code {} with maybe wrong content "{}"'.format(response.status_code,
                                                                                             response.content))
        return response.json()['results']

    def _check_results_length(self, results, query, response_should_be_empty):
        if response_should_be_empty and len(results):
            raise AssertionError('The response should be empty: {}'.format({'url': self._get_endpoint(query),
                                                                            'query': query}))
        elif not response_should_be_empty and not len(results):
            raise AssertionError('empty response {}'.format({'url': self._get_endpoint(query),
                                                             'query': query}))


class UtilsTestCase(TestCase):
    def test_combination_generator(self):
        self.assertEqual(
            (
                (1,), (2,),
                (1, 2)
            ),
            GenericLookupsTestCase._get_combinations((1, 2))
        )
        self.assertEqual(
            (
                (1,), (2,), (3,),
                (1, 2), (1, 3), (2, 3),
                (1, 2, 3),
            ),
            GenericLookupsTestCase._get_combinations((1, 2, 3))
        )


