from django.core.exceptions import ValidationError as DjangoValidationError
from django.http.response import Http404
from rest_framework import mixins
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet


class MultipleRetrieveMixin(object):
    # to support multiple ids separated by comma
    lookup_value_regex = r'\d+(\d+,)*'

    def get_objects(self):
        """
        This is slight modification of rest_framework.generic.GenericAPIView.get_object
        to allow returning multiple objects within single request.
        """
        queryset = self.filter_queryset(self.get_queryset())

        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        kwarg_items = self.kwargs[lookup_url_kwarg]
        kwarg_items = kwarg_items.split(',')
        lookup_field = '%s__in' % self.lookup_field

        filter_kwargs = {lookup_field: kwarg_items}

        # let's create a list (for later processing)
        items = list(queryset.filter(**filter_kwargs))

        # prevent from digging database
        if len(kwarg_items) != len(items):
            raise Http404

        # May raise a permission denied
        for item in items:
            self.check_object_permissions(self.request, item)

        return items

    def retrieve(self, request, *args, **kwargs):
        instances = self.get_objects()

        if 1 < len(instances):
            kwargs = {
                'many': True,
                'instance': instances,
            }

        else:
            kwargs = {
                'instance': instances[0],
            }

        serializer = self.get_serializer(**kwargs)
        return Response(serializer.data)


class ReadOnlyMultipleRetrieveViewSet(
    MultipleRetrieveMixin,
    mixins.ListModelMixin,
    GenericViewSet
):
    """
    `rest_framework.viewsets.ReadOnlyModelViewSet` extended by `MultipleRetrieveMixin`
    """
    pass


class MultipleRetrieveViewSet(
    mixins.CreateModelMixin,
    MultipleRetrieveMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    GenericViewSet
):
    """
    `rest_framework.viewsets.ModelViewSet` extended by `MultipleRetrieveMixin`
    """
    def update(self, request, *args, **kwargs):
        """
        Since some legacy browsers does not support `PATCH` yet, we have
        to handle all incoming `PUT` requests as they would be `PATCH` ones.
        """
        kwargs['partial'] = True
        return super(MultipleRetrieveViewSet, self).update(request, *args, **kwargs)

    def perform_create(self, serializer):
        """
        Adds better error message handling (for JSON requests responds with error
        in JSON format.
        """
        try:
            super(MultipleRetrieveViewSet, self).perform_create(serializer)
        except DjangoValidationError as error:
            raise ValidationError(error.message_dict)

    def perform_update(self, serializer):
        """
        Adds better error message handling (for JSON requests responds with error
        in JSON format.
        """
        try:
            super(MultipleRetrieveViewSet, self).perform_update(serializer)
        except DjangoValidationError as error:
            raise ValidationError(error.message_dict)
