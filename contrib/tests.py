import base64
from datetime import timedelta

from django.conf import settings
from django.core.management import call_command
from django.test import TestCase, Client
from django.utils import timezone
from rest_framework.authtoken.models import Token

from contrib.models import User, TokenActivity

API_BASE = settings.API_BASE


class AccessPermissionTestCase(TestCase):
    def setUp(self):
        self.username = 'testing@user.mail'
        self.password = 'SomeSecretPassword256'
        self.user = User.objects.create_user(self.username, self.username, self.password)
        self.token = Token.objects.create(user=self.user)

    def test_retrieve_token(self):
        auth_string = 'Basic {}'.format(
            base64.b64encode(
                '{username}:{password}'.format(username=self.username,
                                               password=self.password)
                .encode('utf-8')
            ).decode('utf-8')
        )
        client = Client(HTTP_AUTHORIZATION=auth_string)
        res = client.get(f"{API_BASE}token/")
        self.assertEqual(200, res.status_code)
        self.assertEqual({'token': self.token.key}, res.json())

        # wrong credentials
        auth_string = 'Basic {}'.format(
            base64.b64encode(
                '{username}:{password}'.format(username=self.username,
                                               password=self.password + 'xyz')
                    .encode('utf-8')
            ).decode('utf-8')
        )
        client = Client(HTTP_AUTHORIZATION=auth_string)
        res = client.get(f"{API_BASE}token/")
        self.assertEqual(403, res.status_code)
        self.assertEqual({'error': {'type': 'WrongCredentials'}}, res.json())

        # no header
        client = Client()
        res = client.get(f"{API_BASE}token/")
        self.assertEqual(401, res.status_code)
        self.assertEqual({'error': {'type': 'AuthenticationRequired'}}, res.json())

    def test_not_authorized_requests_are_forbidden(self):
        client = Client()
        res = client.get(f"{API_BASE}users/")
        self.assertEqual(401, res.status_code)

    def test_token_authorization_works(self):
        client = Client(HTTP_AUTHORIZATION='Token %s' % self.token)
        res = client.get(f"{API_BASE}users/")
        self.assertEqual(200, res.status_code)

    def test_storing_tokens_last_activity(self):
        client = Client(HTTP_AUTHORIZATION='Token %s' % self.token)

        client.get(f"{API_BASE}users/")
        last_activity = TokenActivity.objects.get(token=self.token).last_activity

        client.get(f"{API_BASE}users/")
        current_activity = TokenActivity.objects.get(token=self.token).last_activity

        self.assertGreater(current_activity, last_activity)

    def test_tokens_get_expired(self):
        # tokens with expired activity get deleted
        #
        token_activity = self.token.activity
        last_activity = timezone.now() - timedelta(seconds=settings.TOKEN_EXPIRE_AFTER_INACTIVITY_SECONDS + 5)
        TokenActivity.objects.filter(id=token_activity.id).update(last_activity=last_activity)

        call_command('expire_tokens')
        with self.assertRaises(Token.DoesNotExist):
            self.token.refresh_from_db()


