from django.core.management import call_command
from django.core.management.base import BaseCommand

from contrib.helpers import task_logger


class Command(BaseCommand):
    def handle(self, **options):
        with task_logger('flush_db'):
            call_command('flush', interactive=False)
            call_command('initialize')  # Making sure there as the admin user used for API calls
