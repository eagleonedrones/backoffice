from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone
from rest_framework.authtoken.models import Token

from contrib.helpers import task_logger
from contrib.models import TokenActivity


class Command(BaseCommand):
    def handle(self, **options):
        with task_logger('expire_tokens'):
            expiration_time = timezone.now() - timedelta(seconds=settings.TOKEN_EXPIRE_AFTER_INACTIVITY_SECONDS)

            # expire rest-service tokens
            Token.objects.filter(pk__in=set(
                TokenActivity.objects.filter(last_activity__lt=expiration_time).values_list('token_id', flat=True)
            )).delete()
