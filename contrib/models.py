from django.contrib.auth.models import User as DjUser
from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from hub.helpers import hub_request


class TokenActivity(models.Model):
    token = models.OneToOneField(Token, on_delete=models.CASCADE, related_name='activity')
    last_activity = models.DateTimeField(auto_now_add=True)


@receiver(post_save, sender=Token)
def create_initial_token_activity(sender, instance=None, **kwargs):
    TokenActivity.objects.get_or_create(token=instance)


class User(DjUser):
    class Meta:
        proxy = True
        ordering = ('id',)

    @classmethod
    def get_dummy(cls, **kwargs):
        kwargs.setdefault('username', kwargs.get('email', 'user@company.com'))
        kwargs['email'] = kwargs['username']

        kwargs.setdefault('password', 'password')

        user = cls.objects.filter(username=kwargs['username']).first()
        if user:
            return user
        return cls.objects.create_user(**kwargs)

    @classmethod
    def get_or_register(cls, email):
        user, created = cls.objects.get_or_create(username=email, email=email)
        if created:
            pass
            # TODO: registration process

        return user

    def register_all_keys(self):
        location_ids = set()
        for role in self.roles.all():
            location_ids = location_ids.union(
                set(role.company.locations.all().values_list('id', flat=True))
            )

        for mobile_user_session in self.sessions.all():
            hub_request('set_device', {
                'id': mobile_user_session.device_id,
                'device_type': mobile_user_session.device_type,
                'locations': list(location_ids),
            })


# TODO: test
@receiver(pre_delete, sender=User)
def remove_all_active_sessions(sender, instance: User = None, **kwargs):
    for mobile_user_session in instance.sessions.all():
        hub_request('remove_device', {
            'id': mobile_user_session.device_id,
        })
