import json
import logging
from contextlib import contextmanager

from rest_framework.renderers import JSONRenderer

from contrib.models import User


def create_dev_superuser():
    email = 'admin@eagle.one'
    if not User.objects.filter(username=email).exists():
        User.objects.create_superuser(email, email, 'admin')
    return User.objects.get(username=email)


def jsonable_dict(data):
    """
    When using Django REST Framework, we're obtaining `validated_data` in `create` and `update`
    view methods. These `validated_data` are not serializable by native JSON library, so we are
    passing the data through DRF's serializer.
    """
    return json.loads(JSONRenderer().render(data).decode('utf-8'))


def string_to_bool(string):
    """
    When sending bool values via JavaScript, they are usually lower-case, while we're not supporting
    others, than Python style `True` or `False` values.
    """
    return {'true': True, 'false': False, 'null': None}[string.lower()]


@contextmanager
def task_logger(task_name):
    logger = logging.getLogger('django')
    logger.info('Starting "{task_name}" task.'.format(task_name=task_name))
    yield logger
    logger.info('Finished "{task_name}" task.'.format(task_name=task_name))
