from django.utils import timezone
from rest_framework.authtoken.models import Token

from contrib.models import TokenActivity


class TokenActivityLogger(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        auth_string = request.META.get('HTTP_AUTHORIZATION', None)
        if auth_string:
            auth_type, string = auth_string.split(' ')
            if auth_type.strip().lower() == 'token':
                token_key = string.strip()
                token = Token.objects.filter(key=token_key).first()
                if token:
                    TokenActivity.objects.update_or_create(token=token, defaults={'last_activity': timezone.now()})

        return self.get_response(request)
