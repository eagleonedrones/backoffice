import base64

from django.contrib.auth import authenticate
from django.http import JsonResponse
from rest_framework.authtoken.models import Token


def get_token(request):
    if 'HTTP_AUTHORIZATION' in request.META:
        user = None
        auth = request.META['HTTP_AUTHORIZATION'].split(' ')
        if len(auth) == 2:
            # NOTE: We are only support basic authentication for now.
            #
            if auth[0].lower() == "basic":
                try:
                    username, password = base64.b64decode(auth[1]).decode('utf-8').split(':')
                    user = authenticate(username=username, password=password)

                except (IndexError, UnicodeDecodeError, TypeError):
                    pass

        if not user:
            return JsonResponse({'error': {'type': 'WrongCredentials'}}, status=403)

    elif not request.user.is_authenticated:
        return JsonResponse({'error': {'type': 'AuthenticationRequired'}}, status=401)

    else:
        user = request.user

    token, _ = Token.objects.get_or_create(user=user)
    return JsonResponse(data={'token': token.key})
