FROM python:alpine3.7

ARG GIT_PRIVATE_KEY
ARG GIT_HOST="bitbucket.org"

RUN mkdir app
WORKDIR /app

RUN apk add --no-cache db-dev

ARG BUILD_DEPENDENCIES="build-base git openssh perl python-dev"

RUN apk add --no-cache ${BUILD_DEPENDENCIES} \
 && python3 -m venv /venv

RUN apk add --no-cache postgresql-dev  # custom dev requirements
RUN apk add --no-cache postgresql-client

# set latest pip version to avoid warning messages
RUN /venv/bin/pip install pip==19.1

COPY requirements.general.txt /requirements.general.txt
RUN /venv/bin/pip install -r /requirements.general.txt

RUN mkdir -p /root/.ssh
RUN ssh-keyscan ${GIT_HOST} >> ~/.ssh/known_hosts

COPY requirements.private.txt /requirements.private.txt
RUN echo ${GIT_PRIVATE_KEY} | perl -pe 's/__x__/\n/g' > /root/.ssh/id_rsa \
 && chmod 600 /root/.ssh/id_rsa \
 && /venv/bin/pip install -r /requirements.private.txt \
 && rm /root/.ssh/id_rsa

RUN apk del postgresql-dev  # custom dev requirements
RUN apk del ${BUILD_DEPENDENCIES}

# Copy files
COPY . .

EXPOSE 8000

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["sh", "docker-entrypoint.sh"]
