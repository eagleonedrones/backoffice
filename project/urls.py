"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt
from rest_framework import routers

from contrib.views import get_token
from entity.views import CompanyViewSet, DeviceViewSet, LocationViewSet, UserViewSet

router = routers.DefaultRouter()
router.register(r'companies', CompanyViewSet)
router.register(r'devices', DeviceViewSet)
router.register(r'locations', LocationViewSet)
router.register(r'users', UserViewSet)
# router.register(r'claim-benefit-change', ClaimBenefitChangeViewSet, base_name='claim-benefit-change')

API_TITLE = 'Backoffice API'
API_DESCRIPTION = 'docs/api_description.md'
# API_DESCRIPTION = render_to_string('docs/api_description.md')


_API_BASE = settings.API_BASE[1:]


urlpatterns = [
    path(f"{_API_BASE}token/", csrf_exempt(get_token), name='get_token'),
    # TODO: path('commands/(?P<command>\w+)/', CommandView.as_view(), name='commands'),
    path(_API_BASE, include(router.urls)),
    path(_API_BASE, include('rest_shortcuts.urls')),
    path('admin/', admin.site.urls),
    # path('docs/', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)),
    path('silk/', include('silk.urls', namespace='silk')),
]
