#!/bin/bash

set -e

# Wait until HUB is up
until wget http://${HUB_HOST}:${HUB_PORT}/version/ -O - > /dev/null; do
  echo "HUB is unavailable - sleeping"
  sleep 1
done

# Wait for Postgres
until PGPASSWORD=${DB_PASSWORD} psql -h ${DB_HOST} -U ${DB_USER} -c '\q' ${DB_NAME}; do
  echo "Postgres is unavailable - sleeping"
  sleep 1
done

# Commands available using `docker-compose run hub [COMMAND]`
case "$1" in
    python)
        echo "Run migrations and collect static files"
        /venv/bin/python3 manage.py migrate
        /venv/bin/python3 manage.py collectstatic --no-input

        /venv/bin/python manage.py shell
    ;;
    test)
        /venv/bin/python manage.py test
    ;;
    *)
        # TODO: gunicorn
        echo "Run migrations and collect static files"
        /venv/bin/python3 manage.py migrate
        /venv/bin/python3 manage.py collectstatic --no-input

        echo "Running Server..."
        /venv/bin/python manage.py runserver ${APP_HOST}:${APP_PORT}
    ;;
esac
