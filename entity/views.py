from django.db.models import QuerySet, Count

from contrib.models import User
from drf_utils.views import MultipleRetrieveViewSet, ReadOnlyMultipleRetrieveViewSet
from entity.models import Company, Device, UserRole, Location, MobileUserSession
from entity.serializers import CompanySerializer, DeviceSerializer, UserSerializer, LocationSerializer, \
    MemberSerializer, MemberWriteSerializer, MobileUserSessionSerializer, MobileUserSessionWriteSerializer


class CompanyViewSet(MultipleRetrieveViewSet):
    """
        Lookup fields: `owner_id`

        Company members at `/companies/{company_id}/members/`

        Request `OPTIONS` for more details
    """
    serializer_class = CompanySerializer
    queryset = Company.objects.all()

    def filter_queryset(self, queryset: QuerySet):
        owner_email = self.request.query_params.get('owner', None)
        if owner_email:
            queryset = queryset.filter(owner__email__in=owner_email.split(','))

        return queryset


class LocationViewSet(MultipleRetrieveViewSet):
    """
        Lookup fields: `company_id`

        Request `OPTIONS` for more details
    """
    serializer_class = LocationSerializer
    queryset = Location.objects.all()

    def filter_queryset(self, queryset: QuerySet):
        company_id = self.request.query_params.get('company_id', None)
        if company_id:
            queryset = queryset.filter(company_id__in=company_id.split(','))

        return queryset


class DeviceViewSet(MultipleRetrieveViewSet):
    """
        Lookup fields: `company_id`, `location_id`, `device_type`

        Device's files at `/devices/{device_id}/files/`

        Request `OPTIONS` for more details
    """
    serializer_class = DeviceSerializer
    queryset = Device.objects.all()

    def get_queryset(self):
        return self.queryset.prefetch_related('location')

    def filter_queryset(self, queryset: QuerySet):
        company_id = self.request.query_params.get('company_id', None)
        if company_id:
            queryset = queryset.filter(location__company_id__in=company_id.split(','))

        location_id = self.request.query_params.get('location_id', None)
        if location_id:
            queryset = queryset.filter(location_id__in=location_id.split(','))

        device_type = self.request.query_params.get('device_type', None)
        if device_type:
            queryset = queryset.filter(device_type__in=device_type.split(','))

        return queryset


class UserViewSet(ReadOnlyMultipleRetrieveViewSet):
    """
        Lookup fields: `company_id`

        User's sessions at `/users/{user_id}/sessions/`

        Shortcut for logged user: `/users/me/`

        Request `OPTIONS` for more details
    """
    serializer_class = UserSerializer
    queryset = User.objects.annotate(roles_count=Count('roles')).filter(roles_count__gte=1)

    # use email as user's id
    lookup_value_regex = r'[^/]+'
    lookup_field = 'email'
    lookup_url_kwarg = 'email'

    def get_objects(self):
        if self.kwargs['email'] == 'me':
            return [self.request.user]

        return super(UserViewSet, self).get_objects()

    def filter_queryset(self, queryset: QuerySet):
        company_id = self.request.query_params.get('company_id', None)
        if company_id:
            queryset = queryset.filter(roles__company_id__in=company_id.split(','))

        return queryset


class MobileUserSessionViewSet(MultipleRetrieveViewSet):
    """
        Request `OPTIONS` for more details
    """
    serializer_class = MobileUserSessionSerializer
    queryset = MobileUserSession.objects.all()

    def _get_user_email(self):
        user_email = self.request.query_params['user_email']
        if user_email == 'me':
            return self.request.user.email
        return user_email

    def get_serializer(self, *args, **kwargs):
        if 'data' not in kwargs:
            return super(MobileUserSessionViewSet, self).get_serializer(*args, **kwargs)

        serializer_class = MobileUserSessionWriteSerializer
        kwargs['context'] = self.get_serializer_context()
        serializer = serializer_class(*args, **kwargs)

        serializer.initial_data = serializer.initial_data.copy()
        serializer.initial_data['user_id'] = User.objects.get(email=self._get_user_email()).pk
        return serializer

    def filter_queryset(self, queryset: QuerySet):
        return queryset.filter(user__email=self._get_user_email())


class MemberViewSet(MultipleRetrieveViewSet):
    """
        Lookup fields: `role`

        Request `OPTIONS` for more details
    """
    serializer_class = MemberSerializer
    queryset = UserRole.objects.all()

    def get_serializer(self, *args, **kwargs):
        if 'data' not in kwargs:
            return super(MemberViewSet, self).get_serializer(*args, **kwargs)

        serializer_class = MemberWriteSerializer
        kwargs['context'] = self.get_serializer_context()
        serializer = serializer_class(*args, **kwargs)

        serializer.initial_data = serializer.initial_data.copy()
        serializer.initial_data['company_id'] = self.request.query_params['company_id']
        return serializer

    def filter_queryset(self, queryset: QuerySet):
        company_id = self.request.query_params['company_id']
        queryset = queryset.filter(company_id=company_id)

        role = self.request.query_params.get('role', None)
        if role:
            queryset = queryset.filter(role__in=role.split(','))

        return queryset
