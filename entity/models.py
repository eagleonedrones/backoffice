from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from rest_framework.exceptions import ValidationError

from contrib.models import User
from hub.helpers import hub_request
from utils.crypting import get_rsa_keys, export_key_pub
from utils.device_id import get_device_id_from_key_pub

OWNER_ROLE = 'owner'
ADMIN_ROLE = 'admin'
MEMBER_ROLE = 'member'


DEVICE_TYPE_EAGLE = 'e'
DEVICE_TYPE_DEDRONE = 'd'
DEVICE_TYPE_MOBILE = 'm'


class Company(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(User, on_delete=models.PROTECT, related_name='owned_companies')

    class Meta:
        ordering = ('id',)
        verbose_name_plural = 'Companies'

    @classmethod
    def get_dummy(cls, **kwargs):
        kwargs.setdefault('name', 'Drone Company')

        if 'owner' in kwargs:
            kwargs['owner_id'] = kwargs.pop('owner').id
        elif 'owner_id' not in kwargs:
            kwargs['owner_id'] = User.get_dummy(username='owner@company.com').id

        return cls.objects.get_or_create(**kwargs)[0]

    def __str__(self):
        return self.name


class UserRole(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='roles')
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='member_roles')
    role = models.CharField(max_length=6, choices=(
        (OWNER_ROLE, 'Owner'),
        (ADMIN_ROLE, 'Admin'),
        (MEMBER_ROLE, 'Member'),
    ))

    class Meta:
        ordering = ('id',)

    @classmethod
    def get_dummy(cls, **kwargs):
        kwargs.setdefault('role', 'member')

        if 'company' in kwargs:
            kwargs['company_id'] = kwargs.pop('company').id
        elif 'company_id' not in kwargs:
            kwargs['company_id'] = Company.get_dummy().id

        if 'user' in kwargs:
            kwargs['user_id'] = kwargs.pop('user').id
        elif 'user_id' not in kwargs:
            kwargs['user_id'] = Company.objects.get(pk=kwargs['company_id']).owner_id

        return cls.objects.get_or_create(**kwargs)[0]

    def save(self, *args, **kwargs):
        self.full_clean()
        return super(UserRole, self).save(*args, **kwargs)

    def clean(self):
        other_roles_to_same_company = self.user.roles.filter(company_id=self.company_id)
        if self.pk:
            other_roles_to_same_company = other_roles_to_same_company.exclude(pk=self.pk)

        if other_roles_to_same_company.exists():
            raise ValidationError('User cannot have multiple Roles in a single Company')


class Location(models.Model):
    name = models.CharField(max_length=50)
    company = models.ForeignKey(Company, on_delete=models.PROTECT, related_name='locations')

    geo_sw_lat = models.DecimalField(max_digits=9, decimal_places=6)
    geo_sw_lng = models.DecimalField(max_digits=9, decimal_places=6)
    geo_ne_lat = models.DecimalField(max_digits=9, decimal_places=6)
    geo_ne_lng = models.DecimalField(max_digits=9, decimal_places=6)

    class Meta:
        ordering = ('id',)

    @classmethod
    def get_dummy(cls, **kwargs):
        kwargs.setdefault('name', 'Císařský Ostrov')
        kwargs.setdefault('geo_sw_lat', 12.3)
        kwargs.setdefault('geo_sw_lng', 12.4)
        kwargs.setdefault('geo_ne_lat', 13.2)
        kwargs.setdefault('geo_ne_lng', 13.3)

        if 'company' in kwargs:
            kwargs['company_id'] = kwargs.pop('company').id
        elif 'company_id' not in kwargs:
            kwargs['company_id'] = Company.get_dummy().id

        return cls.objects.get_or_create(**kwargs)[0]

    def __str__(self):
        return self.name


# TODO: tests
@receiver((post_save, post_delete), sender=Location)
def register_all_member_keys_for_all_locations(sender, instance: Location = None, **kwargs):
    for member_role in instance.company.member_roles.all():
        member_role.user.register_all_keys()


class Client(models.Model):
    public_key = models.TextField()
    device_id = models.CharField(max_length=6, editable=False, unique=True)
    device_type = None

    class Meta:
        ordering = ('id',)

    @property
    def device_type_verbose(self):
        raise NotImplementedError()

    def __str__(self):
        return self.device_id

    def _get_locations(self) -> list:
        raise NotImplementedError()

    def save(self, *args, **kwargs):
        self.device_id = get_device_id_from_key_pub(self.public_key)

        hub_request('set_device', {
            'id': self.device_id,
            'device_type': self.device_type,
            'locations': self._get_locations()
        })
        return super(Client, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        hub_request('remove_device', {
            'id': self.device_id,
        })
        return super(Client, self).delete(*args, **kwargs)


class MobileUserSession(Client):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sessions')
    first_time_usage = models.DateTimeField(auto_now_add=True)
    last_time_usage = models.DateTimeField(auto_now=True)

    device_type = DEVICE_TYPE_MOBILE
    device_type_verbose = 'Mobile App'

    @classmethod
    def get_dummy(cls, **kwargs):
        if 'user' in kwargs:
            kwargs['user_id'] = kwargs.pop('user').id
        elif 'user_id' not in kwargs:
            kwargs['user'] = User.get_dummy()

        if 'public_key' not in kwargs:
            kwargs['public_key'] = export_key_pub(get_rsa_keys(pair_name='dummy_mobile_session')[0])

        return cls.objects.get_or_create(**kwargs)[0]

    def __str__(self):
        return f'[mobile app session] { self.user }'

    def _get_locations(self):
        return list(Location.objects.filter(company__member_roles__user=self.user).values_list('id', flat=True))


# TODO: tests
@receiver(post_save, sender=MobileUserSession)
def register_current_session(sender, instance: MobileUserSession = None, **kwargs):
    instance.user.register_all_keys()


DEVICE_TYPE_CHOICES = (
    (DEVICE_TYPE_EAGLE, 'Eagle'),
    (DEVICE_TYPE_DEDRONE, 'DeDrone'),
)


class Device(Client):
    name = models.CharField(max_length=50)
    location = models.ForeignKey(Location, on_delete=models.PROTECT, related_name='devices')
    device_type = models.CharField(max_length=1, choices=DEVICE_TYPE_CHOICES)

    @property
    def device_type_verbose(self):
        return {i[0]: i[1] for i in DEVICE_TYPE_CHOICES}[self.device_type]

    @classmethod
    def get_dummy(cls, **kwargs):
        kwargs.setdefault('name', 'Eagle')
        kwargs.setdefault('device_type', DEVICE_TYPE_EAGLE)

        if 'location' in kwargs:
            kwargs['location_id'] = kwargs.pop('location').id
        elif 'location_id' not in kwargs:
            kwargs['location_id'] = Location.get_dummy().id

        if 'public_key' not in kwargs:
            kwargs['public_key'] = export_key_pub(get_rsa_keys(pair_name='dummy_device')[0])

        return cls.objects.get_or_create(**kwargs)[0]

    def _get_locations(self):
        return [self.location_id]


# class File(models.Model):
#     device = models.ForeignKey(Device, on_delete=models.CASCADE, related_name='files')
#     filename = models.TextField(max_length=50)
#     file = models.FileField(upload_to=settings.DRONE_FILE_PATH)
#
#     def __str__(self):
#         return f"[{ self.device }] { self.filename }"
#