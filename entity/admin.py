from django.contrib import admin

from entity.models import Company, Location, Device, UserRole

admin.site.register(Company)
admin.site.register(UserRole)
admin.site.register(Location)
admin.site.register(Device)
