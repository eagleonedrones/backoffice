from entity.models import MobileUserSession


def store_mobile_user_public_key(user, public_key):
    session, created = MobileUserSession.objects.get_or_create(user=user, public_key=public_key)
    if not created:
        # update .last_time_usage
        session.save()
