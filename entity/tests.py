from django.conf import settings
from django.test import TestCase
from rest_framework.exceptions import ValidationError

from contrib.models import User
from drf_utils.tests import GenericLookupsTestCase, CrudTestCase
from entity.models import Company, UserRole, Device, Location, MobileUserSession
from utils.crypting import get_rsa_keys, export_key_pub
from utils.device_id import get_device_id_from_key_pub

API_BASE = settings.API_BASE


class TestUserRoles(TestCase):
    def setUp(self):
        self.company = Company.get_dummy()
        self.user = User.get_dummy()

        UserRole.objects.create(role='admin',
                                user=self.user,
                                company=self.company)

    def test_role_in_company(self):
        self.assertEqual(1, UserRole.objects.count())

    def test_multiple_roles_in_different_companies(self):
        another_company = Company.get_dummy(name=f"{ self.company }_another_one")
        UserRole.objects.create(role='member',
                                user=self.user,
                                company=another_company)

        self.assertEqual(2, UserRole.objects.count())
        self.assertEqual(2, self.user.roles.count())

    def test_multiple_roles_in_single_company(self):
        with self.assertRaises(ValidationError):
            UserRole.objects.create(role='member',
                                    user=self.user,
                                    company=self.company)

        self.assertEqual(1, UserRole.objects.count())


class MemberRulesTestCase(CrudTestCase):
    def setUp(self):
        super(MemberRulesTestCase, self).setUp()

        self.role = UserRole.get_dummy(role='admin')

    def test_user_with_roles_in_two_companies(self):
        other_company = Company.get_dummy(name=f"{self.role.company.name}-other")
        response = self.client.post(f"{API_BASE}companies/{other_company.id}/members/", {
            'email': self.role.user.email,
            'role': self.role.role,
        })
        self.assertEqual(201, response.status_code)
        result = response.json()
        self.assertEqual(self.role.user.email, result['email'])
        self.assertEqual(self.role.role, result['role'])

    def test_user_with_two_same_roles_in_a_company(self):
        members = self.client.get(f"{API_BASE}companies/{self.role.company_id}/members/").json()
        response = self.client.post(f"{API_BASE}companies/{self.role.company_id}/members/", {
            'email': self.role.user.email,
            'role': self.role.role,
        })
        self.assertEqual(400, response.status_code)
        self.assertEqual(['User cannot have multiple Roles in a single Company'], response.json())

    def test_user_with_two_different_roles_in_a_company(self):
        response = self.client.post(f"{API_BASE}companies/{self.role.company_id}/members/", {
            'email': self.role.user.email,
            'role': 'member',
        })
        self.assertEqual(400, response.status_code)
        self.assertEqual(['User cannot have multiple Roles in a single Company'], response.json())


TEST_PUBLIC_KEY, _ = get_rsa_keys()


# TODO: location changes device x location HUB's settings


class BasicCrudTestCase(CrudTestCase):
    def setUp(self):
        super(BasicCrudTestCase, self).setUp()

        self.device = Device.get_dummy()
        self.location = self.device.location
        self.company = self.location.company
        UserRole.get_dummy(user=self.user, company=self.company)

        self.session = MobileUserSession.get_dummy(user=self.user)

    def test_user_operations(self):
        current_user = {
            'email': self.user.email,
            'roles': [
                {
                    'id': role.id,
                    'company_id': role.company_id,
                    'role': role.role,
                }
                for role in self.user.roles.all()
            ]
        }

        # read all
        response = self.client.get(f"{API_BASE}users/")
        self.assertEqual(200, response.status_code)
        self.assertLessEqual(1, len(response.json()['results']))

        # read one
        response = self.client.get(f"{API_BASE}users/{self.user.email}/", content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(current_user, response.json())

        # test not allowed methods
        response = self.client.post(f"{API_BASE}users/", {})
        self.assertEqual(405, response.status_code)

        response = self.client.put(f"{API_BASE}users/{self.user.email}/", {})
        self.assertEqual(405, response.status_code)

        response = self.client.patch(f"{API_BASE}users/{self.user.email}/", {})
        self.assertEqual(405, response.status_code)

        response = self.client.delete(f"{API_BASE}users/{self.user.email}/", {})
        self.assertEqual(405, response.status_code)

    def test_user_session_operations(self):
        data = {
            'public_key': export_key_pub(TEST_PUBLIC_KEY),
        }

        # create
        response = self.client.post(f"{API_BASE}users/me/sessions/", data=data)
        self.assertEqual(201, response.status_code)
        new_item = response.json()

        self.assertEqual(get_device_id_from_key_pub(TEST_PUBLIC_KEY), new_item['device_id'])
        self.assertTrue('last_time_usage' in new_item)
        self.assertTrue('first_time_usage' in new_item)

        # read all
        response = self.client.get(f"{API_BASE}users/me/sessions/")
        self.assertEqual(200, response.status_code)
        results = response.json()['results']
        self.assertEqual(self.session.id, results[0]['id'])
        self.assertEqual(self.session.device_id, results[0]['device_id'])
        self.assertEqual(new_item['id'], results[1]['id'])
        self.assertEqual(new_item['device_id'], results[1]['device_id'])

        # read one
        response = self.client.get(f"{API_BASE}users/me/sessions/{new_item['id']}/", content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_item, response.json())

        # read one with `user__email`
        response = self.client.get(f"{API_BASE}users/{self.user.email}/sessions/{new_item['id']}/",
                                   content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_item, response.json())

        # re-create (alter `last_time_usage`)
        _new_item = MobileUserSession.objects.get(pk=new_item['id'])
        response = self.client.post(f"{API_BASE}users/me/sessions/", data=data)
        self.assertEqual(201, response.status_code)
        other_item = response.json()
        _other_item = MobileUserSession.objects.get(pk=other_item['id'])

        self.assertEqual(new_item['device_id'], other_item['device_id'])
        self.assertEqual(new_item['first_time_usage'], other_item['first_time_usage'])

        # test not allowed methods
        response = self.client.put(f"{API_BASE}users/{self.user.email}/", {})
        self.assertEqual(405, response.status_code)

        response = self.client.patch(f"{API_BASE}users/{self.user.email}/", {})
        self.assertEqual(405, response.status_code)

        # delete
        response = self.client.delete(f"{API_BASE}users/me/sessions/{new_item['id']}/", data=new_item)
        self.assertEqual(204, response.status_code)

        # read one (non-existing)
        response = self.client.get(f"{API_BASE}users/me/sessions/{new_item['id']}/", content_type='application/json')
        self.assertEqual(404, response.status_code)

    def test_company_operations(self):
        data = {
            'name': f"{self.company.name} - other",
            'owner': self.company.owner.email,
        }

        # create
        response = self.client.post(f"{API_BASE}companies/", data=data)
        self.assertEqual(201, response.status_code)
        new_item = response.json()

        self.assertEqual(data['name'], new_item['name'])
        self.assertEqual(data['owner'], new_item['owner'])

        # read all
        response = self.client.get(f"{API_BASE}companies/")
        self.assertEqual(200, response.status_code)
        self.assertEqual([
            {
                'id': self.company.id,
                'name': self.company.name,
                'owner': self.company.owner.email,
            },
            new_item,
        ], response.json()['results'])

        # read one
        response = self.client.get(f"{API_BASE}companies/{new_item['id']}/", content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_item, response.json())

        # update
        new_item['name'] += '!'
        response = self.client.put(f"{API_BASE}companies/{new_item['id']}/",
                                   data=new_item,
                                   content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_item, response.json())

        # delete
        response = self.client.delete(f"{API_BASE}companies/{new_item['id']}/", data=new_item)
        self.assertEqual(204, response.status_code)

        # read one (non-existing)
        response = self.client.get(f"{API_BASE}companies/{new_item['id']}/", content_type='application/json')
        self.assertEqual(404, response.status_code)

    def test_location_operations(self):
        data = {
            'company_id': self.company.id,
            'name': f"{self.location.name} - other",
            'geo_sw_lat': self.location.geo_sw_lat + 1,
            'geo_sw_lng': self.location.geo_sw_lng + 1,
            'geo_ne_lat': self.location.geo_ne_lat + 1,
            'geo_ne_lng': self.location.geo_ne_lng + 1,
        }

        # create
        response = self.client.post(f"{API_BASE}locations/", data=data)
        self.assertEqual(201, response.status_code)
        new_item = response.json()

        self.assertEqual(data['company_id'], new_item['company_id'])
        self.assertEqual(data['name'], new_item['name'])
        self.assertEqual(float(data['geo_sw_lat']), float(new_item['geo_sw_lat']))
        self.assertEqual(float(data['geo_sw_lng']), float(new_item['geo_sw_lng']))
        self.assertEqual(float(data['geo_ne_lat']), float(new_item['geo_ne_lat']))
        self.assertEqual(float(data['geo_ne_lng']), float(new_item['geo_ne_lng']))
        self.assertTrue('device_ids' in new_item)

        # read all
        response = self.client.get(f"{API_BASE}locations/")
        self.assertEqual(200, response.status_code)
        results = response.json()['results']
        self.assertEqual(2, len(results))

        self.assertEqual(self.location.id, results[0]['id'])
        self.assertEqual(self.location.name, results[0]['name'])
        self.assertEqual(float(self.location.geo_sw_lat), float(results[0]['geo_sw_lat']))
        self.assertEqual(float(self.location.geo_sw_lng), float(results[0]['geo_sw_lng']))
        self.assertEqual(float(self.location.geo_ne_lat), float(results[0]['geo_ne_lat']))
        self.assertEqual(float(self.location.geo_ne_lng), float(results[0]['geo_ne_lng']))

        self.assertEqual(new_item['id'], results[1]['id'])
        self.assertEqual(new_item['name'], results[1]['name'])
        self.assertEqual(float(new_item['geo_sw_lat']), float(results[1]['geo_sw_lat']))
        self.assertEqual(float(new_item['geo_sw_lng']), float(results[1]['geo_sw_lng']))
        self.assertEqual(float(new_item['geo_ne_lat']), float(results[1]['geo_ne_lat']))
        self.assertEqual(float(new_item['geo_ne_lng']), float(results[1]['geo_ne_lng']))

        # read one
        response = self.client.get(f"{API_BASE}locations/{new_item['id']}/", content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_item, response.json())

        # update
        new_item['name'] += '!'
        response = self.client.put(f"{API_BASE}locations/{new_item['id']}/",
                                   data=new_item,
                                   content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_item, response.json())

        # delete
        response = self.client.delete(f"{API_BASE}locations/{new_item['id']}/", data=new_item)
        self.assertEqual(204, response.status_code)

        # read one (non-existing)
        response = self.client.get(f"{API_BASE}locations/{new_item['id']}/", content_type='application/json')
        self.assertEqual(404, response.status_code)

    def test_device_operations(self):
        data = {
            'location_id': self.location.id,
            'public_key': export_key_pub(TEST_PUBLIC_KEY),
            'device_type': self.device.device_type,
            'name': f"{self.device.name} - other",
        }

        # create
        response = self.client.post(f"{API_BASE}devices/", data=data)
        self.assertEqual(201, response.status_code)
        new_item = response.json()

        self.assertEqual(data['location_id'], new_item['location_id'])
        self.assertEqual(data['device_type'], new_item['device_type'])
        self.assertEqual(data['name'], new_item['name'])
        self.assertEqual(self.device.location.company_id, new_item['company_id'])
        self.assertEqual(get_device_id_from_key_pub(TEST_PUBLIC_KEY), new_item['device_id'])

        # read all
        response = self.client.get(f"{API_BASE}devices/")
        self.assertEqual(200, response.status_code)
        self.assertEqual([
            {
                'id': self.device.id,
                'location_id': self.device.location_id,
                'company_id': self.device.location.company_id,
                'device_type': self.device.device_type,
                'name': self.device.name,
                'device_id': self.device.device_id,
            },
            new_item,
        ], response.json()['results'])

        # read one
        response = self.client.get(f"{API_BASE}devices/{new_item['id']}/", content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_item, response.json())

        # update
        new_item['name'] += '!'
        response = self.client.put(f"{API_BASE}devices/{new_item['id']}/",
                                   data=new_item,
                                   content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_item, response.json())

        # delete
        response = self.client.delete(f"{API_BASE}devices/{new_item['id']}/", data=new_item)
        self.assertEqual(204, response.status_code)

        # read one (non-existing)
        response = self.client.get(f"{API_BASE}devices/{new_item['id']}/", content_type='application/json')
        self.assertEqual(404, response.status_code)


# TODO: store_mobile_user_public_key updates last_time_usage


# TODO: test update device / mobile app

class DeviceLookupTestCase(GenericLookupsTestCase):
    def setUp(self):
        super(DeviceLookupTestCase, self).setUp()

        device = Device.get_dummy()

        self.api_endpoint = f"{API_BASE}devices/"
        self.entity = device
        self.lookup_fields = {'company_id', 'location_id', 'device_type'}
        self.defaults = {
            'company_id': device.location.company_id,
        }


class LocationLookupsTestCase(GenericLookupsTestCase):
    def setUp(self):
        super(LocationLookupsTestCase, self).setUp()

        location = Location.get_dummy()

        self.api_endpoint = f"{API_BASE}locations/"
        self.entity = location
        self.lookup_fields = {'company_id'}


class CompanyLookupTestCase(GenericLookupsTestCase):
    def setUp(self):
        super(CompanyLookupTestCase, self).setUp()

        company = Company.get_dummy()

        self.api_endpoint = f"{API_BASE}companies/"
        self.entity = company
        self.lookup_fields = {'owner'}
        self.defaults = {
            'owner': company.owner.email,
        }


class UserLookupTestCase(GenericLookupsTestCase):
    def setUp(self):
        super(UserLookupTestCase, self).setUp()

        user = User.get_dummy()
        UserRole.objects.update_or_create(user_id=user.id,
                                          company_id=Company.get_dummy().id,
                                          defaults={
                                              'role': 'owner',
                                          })

        self.api_endpoint = f"{API_BASE}users/"
        self.entity = user
        self.lookup_fields = {'company_id'}
        self.defaults = {
            'company_id': user.roles.first().company_id
        }
        self.test_response_attrs = {
            'company_id': lambda c, i, v: c.assertEqual(v, i['roles'][0]['company_id'])
        }


class MemberLookupTestCase(GenericLookupsTestCase):
    def _get_endpoint(self, query):
        company_id = query.pop('company_id', None)
        if company_id:
            # strip off multiple values
            company_id = str(company_id).split(',')[-1]

            return f"{API_BASE}companies/{company_id}/members/"

        else:
            return f"{API_BASE}companies/{self.entity.company_id}/members/"

    def setUp(self):
        super(MemberLookupTestCase, self).setUp()

        role = UserRole.get_dummy()

        self.api_endpoint = None
        self.entity = role
        self.lookup_fields = {'company_id', 'role'}
        self.test_response_attrs = {
            'company_id': lambda c, i, v: c.assertEqual(v, UserRole.objects.get(pk=i['id']).company_id)
        }
