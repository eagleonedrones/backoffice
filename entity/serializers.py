from rest_framework import serializers

from contrib.models import User
from drf_utils.serializers import IdModelSerializer
from entity.models import Location, Company, Device, UserRole, MobileUserSession


class CompanySerializer(IdModelSerializer):
    owner = serializers.EmailField(source='owner.email')

    class Meta:
        model = Company
        fields = ('id', 'name', 'owner',)

    def create(self, validated_data):
        validated_data['owner'] = User.get_or_register(validated_data['owner']['email'])
        return super(CompanySerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        validated_data['owner'] = User.get_or_register(validated_data['owner']['email'])
        return super(CompanySerializer, self).update(instance, validated_data)


class LocationSerializer(IdModelSerializer):
    device_ids = serializers.PrimaryKeyRelatedField(many=True, source='devices', read_only=True)

    class Meta:
        model = Location
        fields = ('id', 'company', 'name', 'geo_sw_lat', 'geo_sw_lng', 'geo_ne_lat', 'geo_ne_lng', 'device_ids')


class ClientSerializer(IdModelSerializer):
    public_key = serializers.CharField(write_only=True)
    device_id = serializers.CharField(read_only=True)


class DeviceSerializer(ClientSerializer):
    company_id = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Device
        fields = ('id', 'company_id', 'location', 'device_id', 'device_type', 'name', 'public_key')

    @staticmethod
    def get_company_id(obj: Device):
        return obj.location.company_id


class RoleSerializer(IdModelSerializer):
    class Meta:
        model = UserRole
        fields = ('id', 'company', 'role')


class UserSerializer(IdModelSerializer):
    roles = RoleSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('email', 'roles',)


class MobileUserSessionSerializer(ClientSerializer):
    class Meta:
        model = MobileUserSession
        fields = ('id', 'first_time_usage', 'last_time_usage', 'public_key', 'device_id')


class MobileUserSessionWriteSerializer(ClientSerializer):
    class Meta:
        model = MobileUserSession
        fields = ('id', 'user', 'first_time_usage', 'last_time_usage', 'public_key', 'device_id')
        extra_kwargs = {
            'user': {'write_only': True},
        }

    def create(self, validated_data):
        return MobileUserSession.objects.update_or_create(**validated_data)[0]


class MemberSerializer(IdModelSerializer):
    email = serializers.EmailField(source='user.email')

    class Meta:
        model = UserRole
        fields = ('id', 'email', 'role')


class MemberWriteSerializer(IdModelSerializer):
    email = serializers.EmailField(source='user.email')

    class Meta:
        model = UserRole
        fields = ('id', 'email', 'company', 'role')
        extra_kwargs = {
            'company': {'write_only': True}
        }

    def create(self, validated_data):
        validated_data['user'] = User.get_or_register(validated_data['user']['email'])
        return super(MemberWriteSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        validated_data['user'] = User.get_or_register(validated_data['user']['email'])
        return super(MemberWriteSerializer, self).update(instance, validated_data)

