import re

from django.core.validators import RegexValidator
from django.db import models

from entity.models import MobileUserSession, DEVICE_TYPE_CHOICES
from hub.helpers import hub_request

path_re = re.compile(r'^[-a-zA-Z0-9_/]+\Z', flags=0)
validate_path = RegexValidator(
    path_re,
    "Enter a valid 'path' consisting of letters, numbers, underscores, hyphens or slashes.",
    'invalid'
)


class MessageType(models.Model):
    path = models.CharField(max_length=30, validators=[validate_path])

    def __str__(self):
        return self.path


class Deliverance(models.Model):
    message_type = models.ForeignKey(MessageType, on_delete=models.CASCADE, related_name='deliverances')
    device_type = models.CharField(max_length=1, choices=(
        (MobileUserSession.device_type, MobileUserSession.device_type_verbose),
    ) + DEVICE_TYPE_CHOICES)

    def __str__(self):
        return f"{ self.message_type_id }-{ self.device_type }"

    def save(self, *args, **kwargs):
        device_types = Deliverance.objects.filter(message_type_id=self.message_type_id) \
                                          .values_list('device_type', flat=True)
        hub_request('set_deliverance', {
            'message_type': self.message_type.path,
            'device_types': device_types,
        })
        return super(Deliverance, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        device_types = Deliverance.objects.filter(message_type_id=self.message_type_id)\
                                          .exclude(pk=self.pk)\
                                          .values_list('device_type', flat=True)

        if device_types:
            hub_request('set_deliverance', {
                'message_type': self.message_type_id,
                'device_types': device_types,
            })
        else:
            hub_request('remove_deliverance', {
                'message_type': self.message_type_id,
            })
        return super(Deliverance, self).delete(*args, **kwargs)
