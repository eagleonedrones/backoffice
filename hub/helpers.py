import os

import requests

# HUB_HOST = os.environ.get('HUB_HOST', '37.157.193.224')
HUB_HOST = os.environ.get('HUB_HOST', '127.0.0.1')
HUB_PORT = os.environ.get('HUB_PORT', '80')


def hub_request(path, data):
    response = requests.post(f"http://{ HUB_HOST }:{ HUB_PORT }/{path}", json=data)
    if response.text != 'OK':
        raise EnvironmentError('HUB not responded with OK')
