from django.urls import path

import rest_shortcuts.views

urlpatterns = [
    path('companies/<int:company_id>/members/', rest_shortcuts.views.companies_to_members),
    path('companies/<int:company_id>/members/<int:member_id>/', rest_shortcuts.views.companies_to_members),
    path('devices/<int:device_id>/files/', rest_shortcuts.views.device_to_file),
    path('devices/(<int:device_id>/files/<int:file_id>/', rest_shortcuts.views.device_to_file),
    path('users/<str:user_email>/sessions/', rest_shortcuts.views.user_to_session),
    path('users/<str:user_email>/sessions/<int:session_id>/', rest_shortcuts.views.user_to_session),
]
