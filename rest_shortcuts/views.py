# TODO: cleanup
# from claim.models import Claim
# from claim.views import ClaimHistoryViewSet
# from django.contrib.contenttypes.models import ContentType
# from policy.views import BeneficiaryViewSet, BeneficiaryBenefitUsageViewSet, BenefitUsageViewSet
# from product.views import BenefitViewSet
# from rest_framework.generics import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from entity.views import MemberViewSet, MobileUserSessionViewSet


# DRF views have `csrf_exempt` applied by default so we have to do so too
@csrf_exempt
def companies_to_members(request, company_id, member_id=None, _format=None):
    request.GET = request.GET.copy()
    request.GET.setlist('company_id', [company_id])

    if member_id:
        return MemberViewSet.as_view(actions={'get': 'retrieve',
                                              'put': 'update',
                                              'patch': 'partial_update',
                                              'delete': 'destroy'})(request, pk=str(member_id), format=_format)
    else:
        return MemberViewSet.as_view(actions={'get': 'list',
                                              'post': 'create'})(request, format=_format)


# DRF views have `csrf_exempt` applied by default so we have to do so too
@csrf_exempt
def device_to_file(request, device_id, file_id=None, _format=None):
    return None


# DRF views have `csrf_exempt` applied by default so we have to do so too
@csrf_exempt
def user_to_session(request, user_email, session_id=None, _format=None):
    request.GET = request.GET.copy()
    request.GET.setlist('user_email', [user_email])

    if session_id:
        return MobileUserSessionViewSet.as_view(actions={'get': 'retrieve',
                                                         'delete': 'destroy'})(request, pk=str(session_id), format=_format)
    else:
        return MobileUserSessionViewSet.as_view(actions={'get': 'list',
                                                         'post': 'create'})(request, format=_format)
