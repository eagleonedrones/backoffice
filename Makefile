.PHONY: build run test clean

build:
	docker-compose -f docker-compose.prod.yaml build \
	 --build-arg GIT_PRIVATE_KEY="$(shell cat ~/.ssh/id_rsa | perl -pe 's/\n/__x__/g')"

run:
	trap 'make clean' EXIT; docker-compose -f docker-compose.prod.yaml up

test:
	trap 'make clean' EXIT; docker-compose -f docker-compose.prod.yaml run backoffice test

clean:
	docker-compose -f docker-compose.prod.yaml down --remove-orphans
